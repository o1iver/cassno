{-
   File	       : $1
   Module      : Cassno.Base
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>

   @TODO: custom read to OutputType (to allow "Cassandra", "cassandra", etc)
-}

{-# LANGUAGE PatternGuards #-}

module Cassno.Base where

import System.IO
import System.Exit
import System.Environment
import System.Console.GetOpt
import Data.List

import Cassno.Types

import qualified Cassno.Reader.Cassandra as CR
import qualified Cassno.Writer.Cassandra as CW
import qualified Cassno.Writer.JSON      as JW
import qualified Cassno.Reader.JSON      as JR

-------------------------------------------------------------------------------
-- Possible command line options
data Flag = Help
          | Dry
          | Check
          | Expand
          | Numeric
          | OutType String
          | InType  String
    deriving (Show,Eq)

-- Options for GetOpt.parse 
options :: [OptDescr Flag]
options = [Option ['h'] ["help"  ] (NoArg Help)
            "show this help message"
          ,Option ['o'] ["output-type"] (ReqArg OutType "output-type")
            "output format [cassandra|json] (default: cassandra)"
          ,Option ['i'] ["input-type"] (ReqArg InType  "input-type")
            "output format [cassandra|json] (default: cassandra)"
          ,Option ['e'] ["expand"] (NoArg Expand)
            "fully expand POMDP definition"
          ,Option ['n'] ["numeric"] (NoArg Numeric)
            "use numering indexing instead of name-based indexing (default: false)"
          ,Option ['c'] ["check" ] (NoArg Check)
            "validate data"
          ,Option [] ["dry" ] (NoArg Dry)
            "dry run (only show configuration)"
          ]

-- Valid output types
validTypes = ["cassno","statements","cassandra","json"]

-- Checks if a Flag constructed with Output is valid
isValidIOType :: Flag -> Bool
isValidIOType (InType t)
    | t `elem` validTypes = True
    | otherwise             = False
isValidIOType (OutType t)
    | t `elem` validTypes = True
    | otherwise             = False

-- Returns the output type string from an Output Flag
getOutput :: Flag -> String
getOutput (OutType o) = o
getOutput (_       ) = error "cannot get output type from non Output data type"

-- Checks if a Flag was constructed with Output
isOutputFlag :: Flag -> Bool
isOutputFlag (OutType _) = True
isOutputFlag (_       ) = False

-- Check if a list of [Flag] countains a Flag of constructed with Ouput
containsOutputFlag :: [Flag] -> Bool
containsOutputFlag = any isOutputFlag

-- Returns the Flag constructed with Output from a list of [Flag]
outputFlag :: [Flag] -> Flag
outputFlag fs = head $ (filter isOutputFlag fs)

-- Returns the Input type string from an Output Flag
getInput :: Flag -> String
getInput (InType o) = o
getInput (_       ) = error "cannot get output type from non Output data type"

-- Checks if a Flag was constructed with Input
isInputFlag :: Flag -> Bool
isInputFlag (InType _) = True
isInputFlag (_       ) = False

-- Check if a list of [Flag] countains a Flag of constructed with Ouput
containsInputFlag :: [Flag] -> Bool
containsInputFlag = any isInputFlag

-- Returns the Flag constructed with Input from a list of [Flag]
inputFlag :: [Flag] -> Flag
inputFlag fs = head $ (filter isInputFlag fs)

-------------------------------------------------------------------------------
-- Run configuration
data RunConfig = RunConfig {
     rcInFile  :: FilePath
    ,rcOutFile :: FilePath
    ,rcOutType :: IOType
    ,rcInType  :: IOType
    ,rcExpand  :: Bool
    ,rcNumeric :: Bool
    ,rcCheck   :: Bool
    ,rcDry     :: Bool
    } deriving (Show)

-- Default RunConfig
defaultRunConfig = RunConfig {
     rcInFile  = "-"
    ,rcOutFile = "-"
    ,rcOutType = Cassandra
    ,rcInType  = Cassandra
    ,rcExpand  = False
    ,rcNumeric = False
    ,rcCheck   = False
    ,rcDry     = False
    }

-- Possible POMDP output file formats
data IOType = Cassno | Statements | Cassandra | JSON
    deriving (Eq)

instance Show IOType where
    show Cassno     = "cassno"
    show Statements = "statements"
    show Cassandra  = "cassandra"
    show JSON       = "json"

-- Adds an inFile to a RunConfig
addInFileToRunConfig :: RunConfig -> FilePath -> RunConfig
addInFileToRunConfig r f = r {rcInFile = f}

-- Adds an outFile to a RunConfig
addOutFileToRunConfig :: RunConfig -> FilePath -> RunConfig
addOutFileToRunConfig r f = r {rcOutFile = f}

-- Creates a new RunConfig using defaultRunConfig and adds [Flag]
flagsToRunConfig :: [Flag] -> RunConfig
flagsToRunConfig = addFlagsToRunConfig defaultRunConfig
 
-- Adds multiple flags to a RunConfig
addFlagsToRunConfig :: RunConfig -> [Flag] -> RunConfig
addFlagsToRunConfig r ([]  ) = r
addFlagsToRunConfig r (f:[]) = addFlagToRunConfig r f
addFlagsToRunConfig r (f:fs) = addFlagsToRunConfig (addFlagToRunConfig r f) fs

-- Adds a Flag to a RunConfig
addFlagToRunConfig :: RunConfig -> Flag -> RunConfig
addFlagToRunConfig r (Check               ) = r {rcCheck   = True}
addFlagToRunConfig r (Expand              ) = r {rcExpand  = True}
addFlagToRunConfig r (Numeric             ) = r {rcNumeric = True}
addFlagToRunConfig r (Dry                 ) = r {rcDry     = True}
addFlagToRunConfig r (OutType "cassno"    ) = r {rcOutType = Cassno}
addFlagToRunConfig r (OutType "statements") = r {rcOutType = Statements}
addFlagToRunConfig r (OutType "cassandra" ) = r {rcOutType = Cassandra}
addFlagToRunConfig r (OutType "json"      ) = r {rcOutType = JSON}
addFlagToRunConfig r (InType "cassno"     ) = r {rcInType  = Cassno}
addFlagToRunConfig r (InType "statements" ) = r {rcInType  = Statements}
addFlagToRunConfig r (InType "cassandra"  ) = r {rcInType  = Cassandra}
addFlagToRunConfig r (InType "json"       ) = r {rcInType  = JSON}
addFlagToRunConfig r (_                   ) = error ("unable to add Flag to " ++
                                                   "RunConfig")


-------------------------------------------------------------------------------
-- Helper functions
parse argv = getOpt Permute options argv
header     = "Usage: cassno [-h] [-i input-type] [-o output-type] [INFILE] [OUTFILE]"
info       = usageInfo header options
dump       = hPutStrLn stderr
die errs   = dump (concat errs' ++ "\n" ++ info) >> exitWith (ExitFailure 1)
    where errs' = map (\e -> "Error: " ++ e) errs
die' errs   = dump (concat errs') >> exitWith (ExitFailure 1)
    where errs' = map (\e -> "Error: " ++ e) errs
help       = dump info


-------------------------------------------------------------------------------
-- Main
main = do
    argv <- getArgs
    case parse argv of
        (opts,files,[]) -> run opts files
        (_,_,errs)      -> die errs 


-------------------------------------------------------------------------------
-- Run
run :: [Flag] -> [String] -> IO ()
run opts files
    -- Check if help required
    | Help `elem` opts  = help

    -- Check if no files defined
    | length files == 0 = die ["no input and output files defined\n"]

    -- Check if no output defined 
    | length files == 1 = die ["no output file defined\n"]

    -- Check if more than two files given
    | length files > 2 = die ["unknown argument(s): " ++
                              concat (intersperse " " $ (tail .tail) files) ++
                              "\n"]

    -- Check if input type is valid
    | not $ or [not $ containsInputFlag opts
               ,isValidIOType $ inputFlag opts] = die ["unknown input type `"
                                                    ++ (getInput . inputFlag)
                                                        opts ++ "'\n"]
    -- Check if output type is valid
    | not $ or [not $ containsOutputFlag opts
               ,isValidIOType $ outputFlag opts] = die ["unknown output type `"
                                                    ++ (getOutput . outputFlag)
                                                        opts ++ "'\n"]
    -- All good, so create run-config and run
    | otherwise         = do
                          let inFile     = head files
                          let outFile    = (head . tail) files 
                          let addInFile  = flip addInFileToRunConfig  inFile
                          let addOutFile = flip addOutFileToRunConfig outFile
                          let rc = (addInFile . addOutFile . flagsToRunConfig)
                                   opts

                          hPutStrLn stderr ""
                          hPutStrLn stderr "-------------------------------------------"
                          hPutStrLn stderr $ "Input file:  " ++ rcInFile  rc
                          hPutStrLn stderr $ "Input type:  " ++ show (rcInType  rc)
                          hPutStrLn stderr $ "Output file: " ++ rcOutFile rc
                          hPutStrLn stderr $ "Output type: " ++ show (rcOutType rc)
                          hPutStrLn stderr $ "Expand:      " ++ show (rcExpand  rc)
                          hPutStrLn stderr $ "Numeric:     " ++ show (rcNumeric rc)
                          hPutStrLn stderr $ "Check:       " ++ show (rcCheck   rc)
                          hPutStrLn stderr $ "Dry:         " ++ show (rcDry     rc)
                          hPutStrLn stderr "-------------------------------------------"
                          hPutStrLn stderr ""

                          if not (rcDry rc)
                            then runWithConfig rc
                            else putStrLn ""

runWithConfig :: RunConfig -> IO ()
runWithConfig rc = do
                   -- Create incoming handle (either stdin or file)
                   inH  <- if rcInFile  rc == "-"
                           then return stdin
                           else openFile (rcInFile  rc) ReadMode
                   -- Create outgoing handle (either stdout or file)
                   outH <- if rcOutFile rc == "-"
                           then return stdout
                           else openFile (rcOutFile rc) WriteMode
                   errH <- return stderr
                   -- Get contents of incoming handle
                   contents' <- hGetContents inH
                   
                   -- Remove values and discount factor
                   let valuesLine   = (unlines . (filter (isPrefixOf "values:"  )) . lines) contents'
                   let discountLine = (unlines . (filter (isPrefixOf "discount:")) . lines) contents'
                   
                   -- Removes the discount and value lines from the content string
                   let contents = (unlines . filter notVNotD . lines) contents'
                            where notVNotD l = not $ or [isPrefixOf "values:" l, isPrefixOf "discount:" l]

                   let useNames = not $ rcNumeric rc

                   let eitherPOMDP = case rcInType rc of
                            Cassandra -> CR.parseStringToPOMDP contents
                            JSON      -> JR.fromStr contents
                            _         -> error $ "currently unsupported input type `"++ show (rcInType rc) ++ "'"

                   case eitherPOMDP of
                        (Left    err) -> hPutStr errH $ "Error: " ++ err
                        (Right pomdp) -> hPutStr outH $ (case rcOutType rc of
                                            Cassandra -> valuesLine ++ discountLine ++ CW.toStr useNames pomdp
                                            JSON      -> JW.toStr pomdp
                                            _         -> error $ "currently unsupported input type `"++ show (rcOutType rc)++"'")
                                                          
                   hClose  inH
                   hClose outH
                   hClose errH

