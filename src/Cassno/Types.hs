{-
   File	       : $1
   Module      : Cassno.Types
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Cassno.Types where

import Data.Maybe
import qualified Data.Map as M
import qualified Data.List as L
import Control.Monad

import Cassno.Util

type Prob   = Double
type Rwrd = Double

--------------------------------------------------------------------------------
-- Type classes

class POMDP p s a o where

    states  :: p s a o -> [s]
    actions :: p s a o -> [a]
    observs :: p s a o -> [o]

    stateName  :: p s a o -> s -> String
    actionName :: p s a o -> a -> String
    observName :: p s a o -> o -> String


    -- Return a transition probability (note: should throw an error when out of
    -- bounds)
    tp  :: p s a o -> a -> s -> s      -> Prob
    
    -- Return an observation probability (note: should throw an error when out
    -- of bounds)
    op  :: p s a o -> a -> s -> o      -> Prob
    
    -- Return a reward (note: should throw an error when out of bounds)
    rw  :: p s a o -> a -> s -> s -> o -> Rwrd

class POMDP p s a o => UpdatablePOMDP p s a o where

    -- Update a transition probability
    updateTP :: p s a o -> a -> s -> s -> Prob -> p s a o
    
    -- Update an observation probability
    updateOP :: p s a o -> a -> s -> o -> Prob -> p s a o
    
    -- Update a reward
    updateRW :: p s a o -> a -> s -> s -> o -> Rwrd -> p s a o

class UpdatablePOMDP p s a o => BuildablePOMDP p s a o where
    
    empty :: p s a o

    setStates  :: p s a o -> [s] -> p s a o
    setActions :: p s a o -> [a] -> p s a o
    setObservs :: p s a o -> [o] -> p s a o

    setStateNames  :: p s a o -> [String] -> p s a o
    setActionNames :: p s a o -> [String] -> p s a o
    setObservNames :: p s a o -> [String] -> p s a o
    
--------------------------------------------------------------------------------
-- Helper functions from POMDP type class

-- Return a list of state names
stateNames  :: POMDP p s a o => p s a o -> [String]
stateNames  p = [stateName  p s | s <- states  p]

-- Return a list of action names
actionNames :: POMDP p s a o => p s a o -> [String]
actionNames p = [actionName p a | a <- actions p]

-- Return a list of observation names
observNames :: POMDP p s a o => p s a o -> [String]
observNames p = [observName p o | o <- observs p]

-- Return the index of a state
stateIx  :: (Eq s,POMDP p s a o) => p s a o -> s -> Maybe Int
stateIx p s  = L.findIndex ((==) s) (states p)

-- Return the index of an action
actionIx :: (Eq a,POMDP p s a o) => p s a o -> a -> Maybe Int
actionIx p a = L.findIndex ((==) a) (actions p)

-- Return the index of an observation
observIx :: (Eq o,POMDP p s a o) => p s a o -> o -> Maybe Int
observIx p o = L.findIndex ((==) o) (observs p)

-- Return the index of an observation given a name
stateIxFromName  :: POMDP p s a o => p s a o -> String -> Maybe Int
stateIxFromName p s  = L.findIndex ((==) s) (stateNames p)

-- Return the index of an action given a name
actionIxFromName :: POMDP p s a o => p s a o -> String -> Maybe Int
actionIxFromName p a = L.findIndex ((==) a) (actionNames p)

-- Return the index of an observation given a name
observIxFromName :: POMDP p s a o => p s a o -> String -> Maybe Int
observIxFromName p o = L.findIndex ((==) o) (observNames p)

-- Uncurried version of tp
tp' :: POMDP p s a o => p s a o -> (a,s,s)          -> Prob
tp' p (a,s,s') = tp p a s s'

-- Uncurried version of op
op' :: POMDP p s a o => p s a o -> (a,s,o)          -> Prob
op' p (a,s,o) = op p a s o

-- Uncurried version of rw
rw' :: POMDP p s a o => p s a o -> (a,s,s,o) -> Rwrd
rw' p (a,s,s',o) = rw p a s s' o

-- Return a transition probability vector given an action and a source state
tpAS :: POMDP p s a o => p s a o -> a -> s -> [Prob]
tpAS p a s = [tp p a s s' | s' <- states  p]

-- Return a transition probability matrix given an action
tpA :: POMDP p s a o => p s a o -> a -> [[Prob]]
tpA p a = [tpAS p a s | s <- states p]

-- Return the entire transition probability cube
tps :: POMDP p s a o => p s a o -> [[[Prob]]]
tps p = [tpA p a | a <- actions p]

-- Return an observation probability vector given an action and a state
opAS :: POMDP p s a o => p s a o -> a -> s -> [Prob]
opAS p a s = [op p a s o  | o  <- observs p]

-- Return an observation probability matrix given an action
opA :: POMDP p s a o => p s a o -> a -> [[Prob]]
opA p a = [opAS p a s | s <- states p]

-- Return the entire observation probability cube
ops :: POMDP p s a o => p s a o -> [[[Prob]]]
ops p = [opA p a | a <- actions p]

-- Return a reward vector given an action, a source state and a sink state
rwASS :: POMDP p s a o => p s a o -> a -> s -> s -> [Rwrd]
rwASS p a s s' = [rw p a s s' o | o <- observs p]

-- Return a reward matrix given an action and a source state
rwAS :: POMDP p s a o => p s a o -> a -> s -> [[Rwrd]]
rwAS p a s = [rwASS p a s s' | s' <- states p]

-- Return a reward cube given an action
rwA :: POMDP p s a o => p s a o -> a -> [[[Rwrd]]]
rwA p a = [rwAS p a s | s <- states p]

-- Return the entire reward 4d-list
rws :: POMDP p s a o => p s a o -> [[[[Rwrd]]]]
rws p = [rwA p a | a <- actions p]

--------------------------------------------------------------------------------
-- Helper functions from UpdatablePOMDP instances 

-- Update a transition probability
updateTP' :: UpdatablePOMDP p s a o => p s a o -> (a,s,s) -> Prob -> p s a o
updateTP' p (a,s,s') = updateTP p a s s'

-- Update an observation probability
updateOP' :: UpdatablePOMDP p s a o => p s a o -> (a,s,o) -> Prob -> p s a o
updateOP' p (a,s,o) = updateOP p a s o

-- Update a reward
updateRW' :: UpdatablePOMDP p s a o => p s a o -> (a,s,s,o) -> Rwrd -> p s a o
updateRW' p (a,s,s',o) = updateRW p a s s' o

-- Update all transition probabilities using a transition-probability cube
updateTPs p prs = foldl update p us
    where us = [(a,s,s', prs !!!! (a,s,s')) |  a  <- actions p
                                              ,s  <- states  p
                                              ,s' <- states  p]
          update p (a,s,s',pr) = updateTP p a s s' pr

-- Update all observations probabilities using a observation-probability cube
updateOPs p prs = foldl update p us
    where us = [(a,s,o, prs !!!! (a,s,o)) |  a <- actions p
                                            ,s <- states  p
                                            ,o <- observs p]
          update p (a,s,o,pr) = updateOP p a s o pr

-- Update all rewards using a 4d reward list 
updateRWs p rws = foldl update p us
    where us = [(a,s,s',o, rws !!!!! (a,s,s',o)) |  a  <- actions p
                                                   ,s  <- states  p
                                                   ,s' <- states  p
                                                   ,o  <- observs p]
          update p (a,s,s',o,rw) = updateRW p a s s' o rw
--------------------------------------------------------------------------------
-- Helper functions from BuildablePOMDP instances 



--------------------------------------------------------------------------------
-- MP POMDP implementation
--
-- Data.Map based POMDP. Since TPs, OBs and RWs are usually sparse using a map
-- may be more efficient than using Array (array) or Matrix (hmatrix). Also the
-- map is implemented using a binary tree, meaning that lookups and updates, etc
-- are O(log n) which is ok.
--
-- Note: there is no point in saving probabilities or rewards equal to 0.0 as
-- tp returns 0.0 when a valid, in terms of index bounds, but non-existent key
-- is used to lookup values.

data MP s a o = MP {
     mpStates  :: [s]
    ,mpActions :: [a]
    ,mpObservs :: [o]
    ,mpStateNames  :: Maybe [String]
    ,mpActionNames :: Maybe [String]
    ,mpObservNames :: Maybe [String]
    ,mpTPs     :: M.Map (a,s,s)   Prob
    ,mpOPs     :: M.Map (a,s,o)   Prob
    ,mpRWs     :: M.Map (a,s,s,o) Rwrd
    } deriving (Show)


mpTP :: (Show s,Show a,Show o,Ord s,Ord a,Ord o) => MP s a o -> a -> s -> s -> Prob
mpTP p a s s'
    | s  `notElem` mpStates  p = error $ "invalid state index `"  ++show s ++ "'"
    | s' `notElem` mpStates  p = error $ "invalid state index `"  ++show s'++ "'"
    | a  `notElem` mpActions p = error $ "invalid action index `" ++show a ++ "'"
    | otherwise = case M.lookup (a,s,s') (mpTPs p) of
        Just pr -> pr
        Nothing -> 0.0

mpOP :: (Show s,Show a,Show o,Ord s,Ord a,Ord o) => MP s a o -> a -> s -> o -> Prob
mpOP p a s o
    | s  `notElem` mpStates  p = error $ "invalid state index `"  ++show s++ "'"
    | a  `notElem` mpActions p = error $ "invalid action index `" ++show a++ "'"
    | o  `notElem` mpObservs p = error $ "invalid observ index `" ++show o++ "'"
    | otherwise = case M.lookup (a,s,o) (mpOPs p) of
        Just pr -> pr
        Nothing -> 0.0

mpRW :: (Show s,Show a,Show o,Ord s,Ord a,Ord o) => MP s a o -> a -> s -> s -> o -> Rwrd
mpRW p a s s' o
    | s  `notElem` mpStates  p = error $ "invalid state index `"  ++show s ++ "'"
    | s' `notElem` mpStates  p = error $ "invalid state index `"  ++show s'++ "'"
    | a  `notElem` mpActions p = error $ "invalid action index `" ++show a ++ "'"
    | o  `notElem` mpObservs p = error $ "invalid observ index `" ++show o ++ "'"
    | otherwise = case M.lookup (a,s,s',o) (mpRWs p) of
        Just rw -> rw
        Nothing -> 0.0

mpStateName :: (Eq s,Show s) => MP s a o -> s -> String
mpStateName p s = names !! index
    where index = case L.findIndex (\s' -> s == s') $ mpStates p of
                    Just ix -> ix
                    Nothing -> error $ "Cassno.Types.MPstateName: invalid state `"++show s++"'"
          names = case mpStateNames p of
                    Just ns -> ns
                    Nothing -> ["state" ++ show s | s <- mpStates p]

mpActionName :: (Eq a,Show a) => MP s a o -> a -> String
mpActionName p a = names !! index
    where index = case L.findIndex (\a' -> a == a') $ mpActions p of
                    Just ix -> ix
                    Nothing -> error $ "Cassno.Types.MPactionName: invalid action `"++show a++"'"
          names = case mpActionNames p of
                    Just ns -> ns
                    Nothing -> ["action" ++ show a | a <- mpActions p]

mpObservName :: (Eq o,Show o) => MP s a o -> o -> String
mpObservName p o = names !! index
    where index = case L.findIndex (\o' -> o == o') $ mpObservs p of
                    Just ix -> ix
                    Nothing -> error $ "Cassno.Types.MPobservName: invalid observ `"++show o++"'"
          names = case mpObservNames p of
                    Just ns -> ns
                    Nothing -> ["observ" ++ show o | o <- mpObservs p]

-- Instance Cassno.Types.POMDP MP
instance (Eq   s,Eq   a,Eq   o
         ,Show s,Show a,Show o
         ,Ord  s,Ord  a,Ord o) => POMDP MP s a o where

    states  = mpStates
    actions = mpActions
    observs = mpObservs

    stateName  = mpStateName
    actionName = mpActionName
    observName = mpObservName

    tp = mpTP
    op = mpOP
    rw = mpRW


mpUpdateTP :: (Ord s,Ord a,Ord o) => MP s a o -> a -> s -> s -> Prob -> MP s a o
mpUpdateTP p a s s' t = p { mpTPs = newTPs }
    where newTPs = M.alter (\_ -> Just t) (a,s,s') oldTPs
          oldTPs = mpTPs p

mpUpdateOP :: (Ord s,Ord a,Ord o) => MP s a o -> a -> s -> o -> Prob -> MP s a o
mpUpdateOP p a s o t = p { mpOPs = newOPs }
    where newOPs = M.alter (\_ -> Just t) (a,s,o) oldOPs
          oldOPs = mpOPs p

mpUpdateRW :: (Ord s,Ord a,Ord o) => MP s a o -> a -> s -> s -> o -> Rwrd -> MP s a o
mpUpdateRW p a s s' o t = p { mpRWs = newRWs }
    where newRWs = M.alter (\_ -> Just t) (a,s,s',o) oldRWs
          oldRWs = mpRWs p


-- Instance Cassno.Types.UpdatablePOMDP MP
instance (Eq   s,Eq   a,Eq   o
         ,Show s,Show a,Show o
         ,Ord  s,Ord  a,Ord  o) => UpdatablePOMDP MP s a o where
    
    updateTP = mpUpdateTP
    updateOP = mpUpdateOP
    updateRW = mpUpdateRW

mpSetStates  :: MP s a o -> [s] -> MP s a o
mpSetStates  p ss = p {mpStates  = ss}

mpSetActions :: MP s a o -> [a] -> MP s a o
mpSetActions p as = p {mpActions = as}

mpSetObservs :: MP s a o -> [o] -> MP s a o
mpSetObservs p os = p {mpObservs = os}

mpSetStateNames :: MP s a o -> [String] -> MP s a o
mpSetStateNames  p sn = p {mpStateNames  = Just sn}

mpSetActionNames :: MP s a o -> [String] -> MP s a o
mpSetActionNames p an = p {mpActionNames = Just an}

mpSetObservNames :: MP s a o -> [String] -> MP s a o
mpSetObservNames p on = p {mpObservNames = Just on}

mpEmpty :: (Ord a,Ord s,Ord o) => MP a s o
mpEmpty = MP {mpStates  = []
             ,mpActions = []
             ,mpObservs = []
             ,mpStateNames  = Nothing
             ,mpActionNames = Nothing
             ,mpObservNames = Nothing
             ,mpTPs = M.empty
             ,mpOPs = M.empty
             ,mpRWs = M.empty}

-- Instance Cassno.Types.BuildablePOMDP MP
instance (Eq   s,Eq   a,Eq   o
         ,Show s,Show a,Show o
         ,Ord s,Ord a,Ord o) => BuildablePOMDP MP s a o where
    
    empty = mpEmpty

    setStates  = mpSetStates
    setActions = mpSetActions
    setObservs = mpSetObservs

    setStateNames  = mpSetStateNames
    setActionNames = mpSetActionNames
    setObservNames = mpSetObservNames



--------------------------------------------------------------------------------
-- MP helper functions

emptyMP :: (Ord a,Ord s,Ord o) => MP a s o
emptyMP = MP {
     mpStates  = []
    ,mpActions = []
    ,mpObservs = []

    ,mpStateNames  = Nothing
    ,mpActionNames = Nothing
    ,mpObservNames = Nothing

    ,mpTPs = M.empty
    ,mpOPs = M.empty
    ,mpRWs = M.empty
    }

--------------------------------------------------------------------------------
-- Example: MP
x1mpStates  = [0,1]
x1mpActions = [0,1]
x1mpObservs = [0,1]
x1mpTPs     = M.fromList [((0,0,0),0.1),((0,0,1),0.9)
                               ,((0,1,0),0.5),((0,1,1),0.5)
                               ,((1,0,0),1.0),((1,0,1),0.0)
                               ,((1,1,0),0.2),((1,1,1),0.8)]
                            
x1mpOPs     = M.fromList [((0,0,0),1.0),((0,0,1),0.0)
                               ,((0,1,0),0.0),((0,1,1),1.0)
                               ,((1,0,0),1.0),((1,0,1),0.0)
                               ,((1,1,0),0.0),((1,1,1),1.0)]

x1mpRWs     = M.fromList [((a,s,s',o),2)|a <- as,s <- ss,s' <- ss,o <- os]
    where ss = x1mpStates
          as = x1mpActions
          os = x1mpObservs

x1MP = MP {
     mpStates  = x1mpStates
    ,mpActions = x1mpActions
    ,mpObservs = x1mpObservs

    ,mpStateNames  = Nothing
    ,mpActionNames = Nothing
    ,mpObservNames = Nothing

    ,mpTPs = x1mpTPs
    ,mpOPs = x1mpOPs
    ,mpRWs = x1mpRWs
    }

x2MP = x1MP {
     mpStateNames  = Just ["left","right"]
    ,mpActionNames = Just ["forward","backward"]
    ,mpObservNames = Just ["moving","stopped"]
    }

