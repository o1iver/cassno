{-
   File	       : $1
   Module      : Cassno.Reader.Cassandra
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module Cassno.Reader.Cassandra where


-- Ugly but don't want to search/replace all names now
import qualified Cassno.Types as T
import Cassno.Util

import Data.List
import Data.Either
import Data.Maybe
import Control.Monad
import Text.ParserCombinators.Parsec hiding (State)


--------------------------------------------------------------------------------
-- Number data type 

data Number = NumInt Int
            | NumDbl Double
            deriving (Show)

numToInt :: Number -> Int
numToInt (NumInt i) = i
numToInt (NumDbl d) = round d

numToDouble :: Number -> Double
numToDouble (NumInt i) = fromIntegral i
numToDouble (NumDbl d) = d

toStr :: Number -> String
toStr (NumInt i) = show i
toStr (NumDbl d) = show d

makeInt :: String -> Number
makeInt s = NumInt $ read s

makeDouble :: String -> String -> Number
makeDouble l r = NumDbl $ read  $ l ++ "." ++ r


--------------------------------------------------------------------------------
-- Data types

type Reward   = Double
type Prob     = Double

-- Different ways of identifying a state
data State = StateIx Int
           | StateName String
           | AnyState

instance Show State where
    show (StateIx  ix) = show ix
    show (StateName n) = n
    show (AnyState   ) = "*"

-- Different ways of identifying an action
data Action = ActionIx Int
            | ActionName String
            | AnyAction

instance Show Action where
    show (ActionIx  ix) = show ix
    show (ActionName n) = n
    show (AnyAction   ) = "*"

-- Different ways of identifying an observation
data Ob = ObIx Int
        | ObName String
        | AnyOb

instance Show Ob where
    show (ObIx  ix) = show ix
    show (ObName n) = n
    show (AnyOb   ) = "*"

-- Different ways of identifying a number of states
data States = NumStates  Int
            | StateNames [String]
            | StateIxs   [Int]
            deriving (Show)

-- Different ways of identifying a number of actions
data Actions = NumActions  Int
             | ActionNames [String]
             deriving (Show)

-- Different ways of identifying a number of observations
data Obs = NumObs  Int
         | ObNames [String]
         deriving (Show)

-- Possible transition probability definitions
data TP = TPASS Action State State Prob     -- T:{a}:{s}:{s'} {pr}\n
        | TPAS  Action State       [Prob]   -- T:{a}:{s}\n{pr0 pr1}\n
        | TPASU Action State                -- T:{a}:{s}\nuniform\n
        | TPA   Action             [[Prob]] -- T:{a}\n{pr00 pr01}\n{pr10 pr11}\n
        | TPAU  Action                      -- T:{a}\nuniform\n
        | TPAI  Action                      -- T:{a}\nidentity\n
        deriving (Show)

-- Possible observation probability definitions
data OP = OPASO Action State Ob Prob     -- O:{a}:{s}:{o} {pr}\n
        | OPAS  Action State    [Prob]   -- O:{a}:{s}\n{pr0 pr1}\n
        | OPASU Action State             -- O:{a}:{s}\nuniform\n
        | OPA   Action          [[Prob]] -- O:{a}\n{pr00 pr01}\n{pr10 pr11}\n
        | OPAU  Action                   -- O:{a}\nuniform\n
        | OPAI  Action                   -- O:{a}\nidentity
        deriving (Show)

-- Possible reward definitions
data RW = RWASSO Action State State Ob Reward     -- R:{a}:{s}:{s'}:{o} {rw}\n
        | RWASS  Action State State    [Reward]   -- R:{a}:{s}:{s'}\n{rw0 rw1}\n
        | RWAS   Action State          [[Reward]] -- R:{a}:{s}\n{rw00 rw01}\n{rw10 rw11}\n
        deriving (Show)

-- Possible start belief definitions
data Start = StartProbs   [Prob]  -- start: 0.4 0.6\n
           | StartUniform         -- start: uniform\n
           | StartByName  String  -- start: state1\n
           | StartByIndex Int -- start: 10\n
           deriving (Show)

-- Possible statements
newtype Stmts = Stmts [Stmt]
data Stmt =
            StmtBlankLine
          | StmtComment String
          | StmtDiscount Double
          | StmtValues   String
          | StmtStates   States
          | StmtActions  Actions
          | StmtObs      Obs
          | StmtStart    Start
          | StmtStartIn  States
          | StmtStartEx  States
          | StmtTP       TP
          | StmtOP       OP
          | StmtRW       RW
          deriving (Show)

--------------------------------------------------------------------------------
-- Basic data type parsers

eol :: Parser ()
eol = char '\n' >> return ()

eol' :: Parser ()
eol' = blanks >> char '\n' >> return ()

blanks :: Parser ()
blanks = many (char ' ') >> return ()

line:: Parser a -> Parser a
line p = do
         r <- p
         optional comment
         eol'
         return r

lineComment :: Parser Stmt
lineComment = line $ char '#' >> many (noneOf "\n") >>= return . StmtComment

comment :: Parser Stmt
comment = char '#' >> many (noneOf "\n") >>= return . StmtComment

blankLine :: Parser Stmt
blankLine = many1 space >> return StmtBlankLine

colon :: Parser ()
colon = blanks >> char ':' >> blanks >> return ()

posInt :: Parser Number
posInt = many1 digit >>= return . makeInt

negInt :: Parser Number
negInt = char '-' >> many1 digit >>= return . NumInt . (\x -> (numToInt x)*(-1)) . makeInt

integer :: Parser Number
integer = posInt <|> negInt

double :: Parser Number
double = do
         left <- integer
         char '.'
         right <- many $ oneOf "0123456789"
         return $ makeDouble (toStr left) right

number :: Parser Number
number = try double
         <|> integer

alias :: Parser String
alias = do
        f <- oneOf "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_."
        rest <-many (oneOf "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_.")
        return $ [f] ++ rest

listOfAliases :: Parser [String]
listOfAliases = sepEndBy1 alias (char ' ')

vector :: Parser [Number]
vector = do 
         v <- sepEndBy1 number (many1 (char ' '))
         optional comment
         return v

vectorOfDoubles :: Parser [Number]
vectorOfDoubles = do 
                  v <- sepEndBy1 double (many1 (char ' '))
                  optional comment
                  return v

vectorOfInts :: Parser [Number]
vectorOfInts = do 
                   v <- sepEndBy1 integer (many1 (char ' '))
                   return v

matrix :: Parser [[Number]]
matrix = endBy1 vector (char '\n')

-- Parser for a single state alias
state :: Parser State
state = (integer >>= return . StateIx . numToInt)
    <|> (alias >>= return . StateName)
    <|> (char '*' >> return AnyState)

-- Parser for a single action alias
action :: Parser Action
action = (integer >>= return . ActionIx . numToInt)
     <|> (alias >>= return . ActionName)
     <|> (char '*' >> return AnyAction)

-- Parser for a single observation alias
ob :: Parser Ob
ob = (integer >>= return . ObIx . numToInt)
 <|> (alias >>= return . ObName)
 <|> (char '*' >> return AnyOb)
--
--------------------------------------------------------------------------------
-- Abstract Parsers

--------------------------------------------------------------------------------
-- Parsers

-- Parse a string until eof returning a number of statements
stmts :: Parser [Stmt]
stmts = do
        ss <- many stmt
        eof
        return ss


stmt :: Parser Stmt
stmt = lineComment
   <|> try discount
   <|> try values
   <|> try actions
   <|> try obs
   <|> try tp
   <|> try op
   <|> try rw
   <|> try states
   <|> try start
   <|> try startInclude
   <|> try startExclude
   <|> blankLine

-- Parse discount statement
discount :: Parser Stmt
discount = line $ string "discount:" >> spaces >> number >>= return . StmtDiscount . numToDouble

-- Parse values statement
values :: Parser Stmt
values = line $ string "values:" >> spaces >> valueType >>= return . StmtValues
    where valueType = try (string "reward") <|> (string "cost"  )

-- Parse state definitions statement
states :: Parser Stmt
states = line $ string "states:" >> spaces >> (try numOf <|> namesOf) >>= return . StmtStates
    where numOf = integer >>= return . NumStates . numToInt
          namesOf = listOfAliases >>= return . StateNames 

-- Parse action definitions statement
actions :: Parser Stmt
actions = line $ string "actions:" >> spaces >> (try numOf <|> namesOf) >>= return . StmtActions
    where numOf = integer >>= return . NumActions . numToInt
          namesOf = listOfAliases >>= return . ActionNames 

-- Parse observation definitions statement
obs :: Parser Stmt
obs = line $ string "observations:" >> spaces >> (try numOf <|> namesOf) >>= return . StmtObs
    where numOf = integer >>= return . NumObs . numToInt
          namesOf = listOfAliases >>= return . ObNames 

-- Parse starting belief definition statement
start :: Parser Stmt
start = line $ string "start:" >> spaces >> (try startProbs
                                         <|> stateIx
                                         <|> try uniform
                                         <|> stateName) >>= return . StmtStart
    where startProbs = vectorOfDoubles >>= return . StartProbs . (map numToDouble)
          uniform    = string "uniform" >> return StartUniform
          stateName  = alias >>= return . StartByName
          stateIx    = integer >>= return . StartByIndex . numToInt

-- Parse starting belief include statement
startInclude :: Parser Stmt
startInclude = line $ string "start include:" >> spaces >> (stateNames
                                                        <|> stateIxs) >>= return . StmtStartIn
    where stateNames = listOfAliases >>= return . StateNames
          stateIxs   = vectorOfInts >>= return . StateIxs . (map numToInt)

-- Parse starting belief exclude statement
startExclude :: Parser Stmt
startExclude = line $ string "start exclude:" >> spaces >> (stateNames
                                                        <|> stateIxs) >>= return . StmtStartEx
    where stateNames = listOfAliases >>= return . StateNames
          stateIxs   = vectorOfInts >>= return . StateIxs . (map numToInt)

-- Parse a transition probability statement
tp :: Parser Stmt
tp = try tp3 <|> try tp2 <|> tp1
 
-- Parse an observation probability statement
op :: Parser Stmt
op = try op3 <|> try op2 <|> op1

-- Parse a reward statement
rw :: Parser Stmt
rw = try rw4 <|> try rw3 <|> rw2

-- Parse a TPASS statement
tp3 = parse3 'T' StmtTP TPASS action state state

-- Parse a TPAS or TPASU statement
tp2 = parse2 'T' StmtTP TPAS TPASU action state

-- Parse TPA, TPAU or TPAI statement
tp1 = parse1 'T' StmtTP TPA TPAU TPAI action

-- Parse a OPASO statement
op3 = parse3 'O' StmtOP OPASO action state ob

-- Parse a OPAS or OPASU statement
op2 = parse2 'O' StmtOP OPAS OPASU action state

-- Parse a OPA, OPAU or OPAI statement
op1 = parse1 'O' StmtOP OPA OPAU OPAU action

-- Parse a line of form: {i}:{p1}:{p2}:{p3} {number}\n
parse3 i c c_ p1 p2 p3 = do
                   blanks
                   char i
                   colon
                   r1 <- p1
                   colon
                   r2 <- p2
                   colon
                   r3 <- p3
                   blanks
                   prob <- number
                   blanks
                   optional comment
                   return $ c (c_ r1 r2 r3 (numToDouble prob))

-- Parse a stmt of form {i}:{p1}:{p2}\n(vector|"uniform")\n
parse2 i c cv cu p1 p2 = do
        blanks
        char i
        colon
        r1 <- p1
        colon
        r2 <- p2
        blanks
        optional comment
        eol'
        vecOrUniform r1 r2
    where vecOrUniform r1 r2 = do
                try (do
                     blanks
                     v <- vector
                     blanks
                     optional comment
                     return (c $ cv r1 r2 (map numToDouble v)))
                <|> (do
                     blanks
                     string "uniform"
                     blanks
                     optional comment
                     return (c $ cu r1 r2))

-- Parse a stmt of form {i}:{p1}:{p2}\n(matrix|"uniform"|"identity")\n
parse1 i c cm cu ci p1 = do
        blanks
        char i
        colon
        r1 <- p1
        blanks
        optional comment
        eol'
        matrixOrUniformOrIdentity r1
    where matrixOrUniformOrIdentity r1 = do
                    try (do
                         m <- matrix
                         return (c $ cm r1 (map (map numToDouble) m)))
                <|> try (do
                         blanks
                         string "uniform"
                         blanks
                         optional comment
                         return (c $ cu r1))
                <|>     (do
                         blanks
                         string "identity"
                         blanks
                         optional comment
                         return (c $ ci r1))

-- Parse a RWASSO statement of form: R:{a}:{s}:{s'}:{o} {reward}\n
rw4 = do
      blanks
      char 'R'
      colon
      a  <- action
      colon
      s  <- state
      colon
      s_ <- state
      colon
      o  <- ob
      blanks
      prob <- number
      blanks
      optional comment
      eol'
      return (StmtRW $ RWASSO a s s_ o (numToDouble prob))

-- Parse a RWASS statement of form R:{a}:{s}:{s'}\n{vector}\n
rw3 = do
      blanks
      char 'R'
      colon
      a  <- action
      colon
      s  <- state
      colon
      s_ <- state
      blanks
      optional comment
      eol'
      blanks
      v <- vector
      blanks
      optional comment
      return (StmtRW $ RWASS a s s_ (map numToDouble v))

-- Parse a RWAS statement of form R:{a}:{s}\n{matrix}\n
rw2 = do
      blanks
      char 'R'
      colon
      a  <- action
      colon
      s  <- state
      blanks
      optional comment
      eol'
      m <- matrix
      return (StmtRW $ RWAS a s (map (map numToDouble) m))

--------------------------------------------------------------------------------
-- Make Cassno.Types.POMDP



isStmtBlankLine s@(StmtBlankLine {}) = True
isStmtBlankLine _                    = False
isStmtComment   s@(StmtComment   {}) = True
isStmtComment   _                    = False
isStmtDiscount  s@(StmtDiscount  {}) = True
isStmtDiscount  _                    = False
isStmtValues    s@(StmtValues    {}) = True
isStmtValues    _                    = False
isStmtStates    s@(StmtStates    {}) = True
isStmtStates    _                    = False
isStmtActions   s@(StmtActions   {}) = True
isStmtActions   _                    = False
isStmtObs       s@(StmtObs       {}) = True
isStmtObs       _                    = False
isStmtStart     s@(StmtStart     {}) = True
isStmtStart     _                    = False
isStmtStartIn   s@(StmtStartIn   {}) = True
isStmtStartIn   _                    = False
isStmtStartEx   s@(StmtStartEx   {}) = True
isStmtStartEx   _                    = False
isStmtTP        s@(StmtTP        {}) = True
isStmtTP        _                    = False
isStmtOP        s@(StmtOP        {}) = True
isStmtOP        _                    = False
isStmtRW        s@(StmtRW        {}) = True
isStmtRW        _                    = False

-- type POMDP  = T.BuildablePOMDP p s a o => p Int Int Int


getStates :: [Stmt] -> States
getStates stmts
    | length ss == 0 = error "states not defined"
    | length ss >= 2 = error "more than one states statement"
    | otherwise = fromStmt $ head ss
    where ss = filter isStmtStates stmts
          fromStmt (StmtStates x) = x

-- setStates :: T.BuildablePOMDP p Int a o => p Int a o -> [Stmt] -> p Int a o
setStates p stmts = case stateNames of 
            [] ->                                 (T.setStates p states)
            ns -> flip T.setStateNames stateNames (T.setStates p states)
    where statesDef = getStates stmts
          (states,stateNames) = tf statesDef
          tf (NumStates   n) = ([0..n-1],[])
          tf (StateNames ns) = ([0..(length ns)-1],ns)
          tf (StateIxs    _) = error "shouldn't have gotten here!"

getActions :: [Stmt] -> Actions
getActions stmts
    | length ss == 0 = error "actions not defined"
    | length ss >= 2 = error "more than one actions statement"
    | otherwise = fromStmt $ head ss
    where ss = filter isStmtActions stmts
          fromStmt (StmtActions x) = x

-- setActions :: T.BuildablePOMDP p s Int o => p s Int o -> [Stmt] -> p s Int o
setActions p stmts = case actionNames of 
            [] ->                                   (T.setActions p actions)
            ns -> flip T.setActionNames actionNames (T.setActions p actions)
    where actionsDef = getActions stmts
          (actions,actionNames) = tf actionsDef
          tf (NumActions   n) = ([0..n-1],[])
          tf (ActionNames ns) = ([0..(length ns)-1],ns)

getObs :: [Stmt] -> Obs
getObs stmts
    | length ss == 0 = error "observations not defined"
    | length ss >= 2 = error "more than one observations statement"
    | otherwise = fromStmt $ head ss
    where ss = filter isStmtObs stmts
          fromStmt (StmtObs x) = x

-- setObs :: T.BuildablePOMDP p s a Int => p s a Int -> [Stmt] -> p s a Int
setObs p stmts = case observNames of 
            [] ->                                   (T.setObservs p observs)
            ns -> flip T.setObservNames observNames (T.setObservs p observs)
    where observsDef = getObs stmts
          (observs,observNames) = tf observsDef
          tf (NumObs   n) = ([0..n-1],[])
          tf (ObNames ns) = ([0..(length ns)-1],ns)

toStateIxs _ (StateIx   i) = [i]
toStateIxs p (StateName n) = case T.stateIxFromName p n of
    Just i  -> [i]
    Nothing -> error $ "illegal state identifier `" ++ n ++ "'"
toStateIxs p (AnyState   ) = T.states p

toActionIxs _ (ActionIx   i) = [i]
toActionIxs p (ActionName n) = case T.actionIxFromName p n of
    Just i  -> [i]
    Nothing -> error $ "illegal action identifier `" ++ n ++ "'"
toActionIxs p (AnyAction   ) = T.actions p

toObservIxs _ (ObIx   i) = [i]
toObservIxs p (ObName n) = case T.observIxFromName p n of
    Just i  -> [i]
    Nothing -> error $ "illegal action identifier `" ++ n ++ "'"
toObservIxs p (AnyOb   ) = T.observs p

--------------------------------------------------------------------------------
-- Set TPs

-- setTPs :: POMDP' -> [Stmt] -> POMDP'

-- setTP :: POMDP' -> TP -> POMDP'
setTP p (TPASS a s s' pr) =
        foldl (\p' ix -> T.updateTP' p' ix pr) p us
    where us = [(a_,s_,s_') | a_ <- as, s_ <- ss, s_' <- ss']
          as  = toActionIxs p a
          ss  = toStateIxs  p s
          ss' = toStateIxs  p s'
setTP p (TPAS a s pr) = foldl setTP p expandedTPs
    where expandedTPs = [createTPASS s' | s' <- T.states p]
          createTPASS s' = TPASS a s (StateIx s') (pr !! s')
setTP p (TPASU a s) = setTP p (TPAS a s uprs)
    where uprs = normalize $ [1.0 | x <- T.states p]
setTP p (TPA a pr) = foldl setTP p expandedTPs
    where expandedTPs = [createTPAS s | s <- T.states p]
          createTPAS s = TPAS a (StateIx s) (pr !! s)
setTP p (TPAU a)   = foldl setTP p tpasus
    where tpasus = [(TPASU a (StateIx s)) | s <- (T.states p)]
setTP p (TPAI a)   = setTP p (TPA a prs)
    where prs = diagOnes (length $ T.states p)

setTPStmt p (StmtTP s) = setTP p s

setTPStmts p stmts = foldl setTPStmt p stmts'
    where stmts' = filter isStmtTP stmts

--------------------------------------------------------------------------------
-- Set OPs

setOP p (OPASO a s o pr) =
        foldl (\p' ix -> T.updateOP' p' ix pr) p us
    where us = [(a_,s_,o_) | a_ <- as, s_ <- ss, o_ <- os]
          as = toActionIxs p a
          ss = toStateIxs  p s
          os = toObservIxs p o
setOP p (OPAS a s pr) = foldl setOP p expandedOPs
    where expandedOPs = [createOPASO o | o <- T.observs p]
          createOPASO o = OPASO a s (ObIx o) (pr !! o)
setOP p (OPASU a s) = setOP p (OPAS a s uprs)
    where uprs = normalize $ [1.0 | o <- T.observs p]
setOP p (OPA a pr) = foldl setOP p expandedOPs
    where expandedOPs = [createOPAS s | s <- T.states p]
          createOPAS s = OPAS a (StateIx s) (pr !! s)
setOP p (OPAU a)   = foldl setOP p opasus
    where opasus = [(OPASU a (StateIx s)) | s <- (T.states p)]
setOP p (OPAI a)   = setOP p (OPA a prs)
    where prs = diagOnes (length $ T.states p)

setOPStmt p (StmtOP s) = setOP p s

setOPStmts p stmts = foldl setOPStmt p stmts'
    where stmts' = filter isStmtOP stmts

--------------------------------------------------------------------------------
-- Set RWs

setRW p (RWASSO a s s' o rw) =
        foldl (\p' ix -> T.updateRW' p' ix rw) p us
    where us = [(a_,s_,s_',o_) | a_ <- as, s_ <- ss, s_' <- ss', o_ <- os]
          as  = toActionIxs p a
          ss  = toStateIxs  p s
          ss' = toStateIxs  p s'
          os  = toObservIxs p o
setRW p (RWASS a s s' rw) = foldl setRW p expandedRWs
    where expandedRWs = [createRWASSO o | o <- T.observs p]
          createRWASSO o = RWASSO a s s' (ObIx o) (rw !! o)
setRW p (RWAS a s rw) = foldl setRW p expandedRWs
    where expandedRWs = [createRWASS s' | s' <- T.states p]
          createRWASS s' = RWASS a s (StateIx s') (rw !! s')

setRWStmt p (StmtRW s) = setRW p s

setRWStmts p stmts = foldl setRWStmt p stmts'
    where stmts' = filter isStmtRW stmts

-- Call after setting state, actions and observations to check for invalid TP,
-- OP or RW statements.
findStmtsErrs p stmts = if any isJust stmtErrs
                        then head $ filter isJust stmtErrs
                        else Nothing
    where stmtErrs = map (findStmtErr p) stmts

-- Check a statement for an error
findStmtErr p (StmtTP s) = findTPError p s
findStmtErr p (StmtOP s) = findOPError p s
findStmtErr p (StmtRW s) = findRWError p s
findStmtErr _ _          = Nothing

validProb :: Double -> Bool
validProb p = and [ge0 p, le1 p]
    where ge0 p = p >= 0.0
          le1 p = p <= 1.0

-- Find error in TP statement
findTPError p (TPASS a s s' pr)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findStateIndexError  p s' = findStateIndexError  p s'
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | not $ validProb pr = Just $ "probability `" ++ show pr ++ "' invalid"
    | otherwise = Nothing
findTPError p (TPAS a s prs)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | any (not . validProb) prs = Just $ ("invalid probability in vector `"
                                           ++ show prs ++ "'")
    | otherwise = Nothing
findTPError p (TPASU a s)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
findTPError p (TPA a prs)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | any (not . validProb) $ concat prs = Just $ ("invalid probability in "
                                                   ++ "matrix `" ++ show prs
                                                   ++ "'")
    | otherwise = Nothing
findTPError p (TPAU a)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
findTPError p (TPAI a)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
    
-- Find error in OP statement
findOPError p (OPASO a s o pr)
    | isJust $ findStateIndexError  p s = findStateIndexError  p s
    | isJust $ findObservIndexError p o = findObservIndexError p o
    | isJust $ findActionIndexError p a = findActionIndexError p a
    | not $ validProb pr = Just $ "probability `" ++ show pr ++ "' invalid"
    | otherwise = Nothing
findOPError p (OPAS a s prs)
    | isJust $ findStateIndexError  p s = findStateIndexError  p s
    | isJust $ findActionIndexError p a = findActionIndexError p a
    | any (not . validProb) prs = Just $ ("invalid probability in vector `"
                                           ++ show prs ++ "'")
    | otherwise = Nothing
findOPError p (OPASU a s)
    | isJust $ findStateIndexError  p s = findStateIndexError  p s
    | isJust $ findActionIndexError p a = findActionIndexError p a
    | otherwise = Nothing
findOPError p (OPA a prs)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | any (not . validProb) $ concat prs = Just $ ("invalid probability in "
                                                   ++ "matrix `" ++ show prs
                                                   ++ "'")
    | otherwise = Nothing
findOPError p (OPAU a)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
findOPError p (OPAI a)
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing

-- Find error in RW statement
findRWError p (RWASSO a s s' o _)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findStateIndexError  p s' = findStateIndexError  p s'
    | isJust $ findObservIndexError p o  = findObservIndexError p o
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
findRWError p (RWASS a s s' _)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findStateIndexError  p s' = findStateIndexError  p s'
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing
finRWError p (RWAS a s _)
    | isJust $ findStateIndexError  p s  = findStateIndexError  p s
    | isJust $ findActionIndexError p a  = findActionIndexError p a
    | otherwise = Nothing

-- Find error with state index
findStateIndexError p (StateIx  ix)
    | ix < 0                  = Just $ "state index `" ++ show ix ++ "' < 0"
    | ix `notElem` T.states p = Just $ "state index `" ++ show ix ++ "' invalid"
    | otherwise              = Nothing
findStateIndexError p (StateName n) = case T.stateIxFromName p n of
    Nothing -> Just $ "unknown state name `" ++ n ++ "'"
    Just ix -> Nothing
findStateIndexError p (AnyState   ) = Nothing

-- Find error with action index
findActionIndexError p (ActionIx  ix)
    | ix < 0                   = Just $ "action index `" ++ show ix ++ "' < 0"
    | ix `notElem` T.actions p = Just $ ("action index `" ++ show ix
                                        ++ "' invalid")
    | otherwise              = Nothing
findActionIndexError p (ActionName n) = case T.actionIxFromName p n of
    Nothing -> Just $ "unknown action name `" ++ n ++ "'"
    Just ix -> Nothing
findActionIndexError p (AnyAction   ) = Nothing

-- Find error with observation index
findObservIndexError p (ObIx  ix)
    | ix < 0                   = Just $ "observation index `" ++ show ix ++ "' < 0"
    | ix `notElem` T.observs p = Just $ ("observation index `" ++ show ix 
                                        ++ "' invalid")
    | otherwise              = Nothing
findObservIndexError p (ObName n) = case T.observIxFromName p n of
    Nothing -> Just $ "unknown observation name `" ++ n ++ "'"
    Just ix -> Nothing
findObservIndexError p (AnyOb   ) = Nothing


-- Make a POMDP from a list of statements
-- makePOMDP :: (T.BuildablePOMDP p s a o) => [Stmt] -> Either String (p s a o)
makePOMDP' sp stmts = case errs of
         Just err -> Left err
         Nothing  -> Right $ (setTPs' . setOPs' . setRWs') bp
     where setStates'     = flip setStates   stmts
           setActions'    = flip setActions  stmts
           setObs'        = flip setObs      stmts
           setTPs'        = flip setTPStmts  stmts
           setOPs'        = flip setOPStmts  stmts
           setRWs'        = flip setRWStmts  stmts
           bp = (setStates' . setActions' . setObs') sp
           errs = findStmtsErrs bp stmts
 
-- makePOMDP :: (T.BuildablePOMDP p s a o) => [Stmt] -> Either String (p s a o)
-- makePOMDP :: (T.BuildablePOMDP p s a o) => [Stmt] -> Either String (p Int Int Int)
-- makePOMDP :: (T.BuildablePOMDP p Int Int Int) => [Stmt] -> Either String (p Int Int Int)
-- makePOMDP = makePOMDP' T.empty
makePOMDP = makePOMDP' T.mpEmpty

testStmts :: [Stmt]
testStmts = [StmtBlankLine
            ,StmtComment "comment"
            ,StmtActions $ NumActions 2
            ,StmtStates  $ StateNames ["left","right"]
            ,StmtObs     $ ObNames ["ob1","ob2"]
            ,StmtTP      $ TPASS AnyAction (StateIx 0) (StateName "right") 0.8
            ,StmtRW      $ RWASSO AnyAction AnyState AnyState AnyOb 10.0
            ]

--------------------------------------------------------------------------------
-- Parser runners

run :: Show a => Parser a -> String -> IO ()
run p input = case (parse p "" input) of
                Left err -> putStr "parse error at " >> print err
                Right x  -> print x

parseStmt :: String -> Either ParseError Stmt
parseStmt input = parse stmt "" input

parseStmts :: String -> Either ParseError [Stmt]
parseStmts input = parse stmts "" input

parseFile :: String -> IO (Either ParseError [Stmt])
parseFile fp = do
               input <- readFile fp
               return $ parseStmts input

parseFileToStmtFile :: FilePath -> FilePath -> IO ()
parseFileToStmtFile inFile outFile = do
                                 result <- parseFile inFile
                                 case result of
                                    Left err -> print err
                                    Right x -> stmtsToFile x
    where stmtsToFile :: [Stmt] -> IO ()
          stmtsToFile ss = writeFile outFile $ unlines (map show ss)

-- parseFileToPOMDP :: FilePath -> IO (Either String POMDP')
parseFileToPOMDP inFile = do
                    parseResult <- parseFile inFile
                    case parseResult of
                        Left  err   -> return (Left $ show err)
                        Right stmts -> return $ makePOMDP stmts

-- parseStringToPOMDP :: String -> Either
parseStringToPOMDP input = case parseStmts input of
        Left  err   -> Left $ show err
        -- Right stmts -> return $ makePOMDP stmts
        Right stmts -> case makePOMDP stmts of
                Left err -> Left $ show err
                Right pomdp -> Right pomdp
