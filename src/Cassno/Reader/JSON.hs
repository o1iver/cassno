{-
   File	       : $1
   Module      : Cassno.Reader.JSON
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module Cassno.Reader.JSON where


import Cassno.Types
import Cassno.Util

import Data.List
import Data.Either
import Data.Maybe
import Control.Monad

import Text.JSON

compose :: [a -> a] -> a -> a
compose fs v = foldl (flip (.)) id fs $ v

-- fromStr :: BuildablePOMDP p a s o => String -> Either String (p a s o)
fromStr str = case decodeStrict str of
    Error err -> Left err
    Ok   json -> case fromJSON json of
                    Error err -> Left  err
                    Ok  pomdp -> Right pomdp

-- fromJSON :: BuildablePOMDP p a s o => JSObject JSValue -> Result (p a s o)
fromJSON obj = do
           stateNames'  <- (<) "state_names"  :: Result [String]
           actionNames' <- (<) "action_names" :: Result [String]
           observNames' <- (<) "observ_names" :: Result [String]
           tps'         <- (<) "tps"          :: Result [[[Prob]]]
           ops'         <- (<) "ops"          :: Result [[[Prob]]]
           rws'         <- (<) "rws"          :: Result [[[[Rwrd]]]]
           let states'  = [0..(length  stateNames')-1]
           let actions' = [0..(length actionNames')-1]
           let observs' = [0..(length observNames')-1]
           -- let pomdp = compose [sSs ,sAs ,sOs
           --                     ,sSNs,sANs,sONs
           --                     ,sTPs,sOPs,sRWs] empty
           let pomdp = compose [flip setStates  states'
                               ,flip setActions actions'
                               ,flip setObservs observs'
                               ,flip setStateNames  stateNames'
                               ,flip setActionNames actionNames'
                               ,flip setObservNames observNames' 
                               ,flip updateTPs tps'
                               ,flip updateOPs ops'
                               ,flip updateRWs rws'] empty
           return pomdp
        where (<) :: JSON a => String -> Result a
              (<) = flip valFromObj obj


--------------------------------------------------------------------------------
-- Parser runners

-- run :: Show a => Parser a -> String -> IO ()
-- run p input = case (parse p "" input) of
--                 Left err -> putStr "parse error at " >> print err
--                 Right x  -> print x
-- 
-- parseStmt :: String -> Either ParseError Stmt
-- parseStmt input = parse stmt "" input
-- 
-- parseStmts :: String -> Either ParseError [Stmt]
-- parseStmts input = parse stmts "" input
-- 
-- parseFile :: String -> IO (Either ParseError [Stmt])
-- parseFile fp = do
--                input <- readFile fp
--                return $ parseStmts input
-- 
-- parseFileToStmtFile :: FilePath -> FilePath -> IO ()
-- parseFileToStmtFile inFile outFile = do
--                                  result <- parseFile inFile
--                                  case result of
--                                     Left err -> print err
--                                     Right x -> stmtsToFile x
--     where stmtsToFile :: [Stmt] -> IO ()
--           stmtsToFile ss = writeFile outFile $ unlines (map show ss)
-- 
-- -- parseFileToPOMDP :: FilePath -> IO (Either String POMDP')
-- parseFileToPOMDP inFile = do
--                     parseResult <- parseFile inFile
--                     case parseResult of
--                         Left  err   -> return (Left $ show err)
--                         Right stmts -> return $ makePOMDP stmts
-- 
-- -- parseStringToPOMDP :: String -> Either
-- parseStringToPOMDP input = case parseStmts input of
--         Left  err   -> Left $ show err
--         -- Right stmts -> return $ makePOMDP stmts
--         Right stmts -> case makePOMDP stmts of
--                 Left err -> Left $ show err
--                 Right pomdp -> Right pomdp
