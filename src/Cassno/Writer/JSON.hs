{-
   File	       : $1
   Module      : Cassno.Writer.JSON
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module Cassno.Writer.JSON where

import Data.List
import Control.Applicative
import Text.JSON
import Text.JSON.Pretty

import Cassno.Types

toStr :: POMDP p s a o => p s a o -> String
toStr = render . pp_js_object . toJSON 

toJSON :: POMDP p s a o => p s a o -> JSObject JSValue
toJSON p = toJSObject [
             ("state_names"  ,showJSON $ stateNames  p)
            ,("action_names" ,showJSON $ actionNames p)
            ,("observ_names" ,showJSON $ observNames p)
            ,("tps"          ,showJSON $ tps         p)
            ,("ops"          ,showJSON $ ops         p)
            ,("rws"          ,showJSON $ rws         p)
            ]

    


