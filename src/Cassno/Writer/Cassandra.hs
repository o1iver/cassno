{-
   File	       : $1
   Module      : Cassno.Writer.Cassandra
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module Cassno.Writer.Cassandra where

import Data.List
import Control.Applicative

import Cassno.Types

import Text.Printf

cis xs = concat (intersperse " " xs)

-- Convert a POMDP to a Cassandra representation string using either the names defined
-- for states, actions and observations or their indexes
toStr :: (POMDP p s a o,Show s, Show a, Show o) => Bool -> p s a o -> String
toStr useNames p = concat $ conversions' <*> [p]
    where conversions' = map(\c -> c useNames) conversions

-- List of required conversions to get to a Cassandra representation of the given POMDP
conversions :: (POMDP p s a o,Show s,Show a,Show o) => [Bool -> p s a o -> String]
conversions = [statesToStr,actionsToStr,observsToStr,tpsToStr,opsToStr,rwsToStr]


-- Helper method used to create statesToStr, actionsToStr and observsToStr
aliasesToStr :: (Show k,POMDP p s a o) =>
    (p s a o -> [k])      ->
    (p s a o -> [String]) ->
    String                ->
    Bool                  ->
    p s a o               ->
    String
aliasesToStr getValues getNames alias useNames p = 
    concat $ [alias, ": "
             ,(if useNames
               then cis $            getNames p
               else cis $ map show $ getValues p)
             ,"\n"]

-- Write states to a Cassandra-style string
statesToStr  :: (Show s,POMDP p s a o) => Bool -> p s a o -> String
statesToStr  = aliasesToStr ((flip (:) []).length.states)  stateNames  "states"

-- Write actions to a Cassandra-style string
actionsToStr :: (Show a,POMDP p s a o) => Bool -> p s a o -> String
actionsToStr = aliasesToStr ((flip (:) []).length.actions) actionNames "actions"
--
-- Write observations to a Cassandra-style string
observsToStr :: (Show o,POMDP p s a o) => Bool -> p s a o -> String
observsToStr = aliasesToStr ((flip (:) []).length.observs) observNames "observations"

-- Write transition probabilities to a Cassandra-style string
tpsToStr :: (POMDP p s a o,Show s, Show a) => Bool -> p s a o -> String
tpsToStr useNames p = concatMap mStr ixs
    where  ixs = [(a,s,s') | a  <- actions p
                           , s  <- states  p 
                           , s' <- states  p]
           mStr ix@(a,s,s') = if not useNames
                           then ("T:"++show a++":"++show s++":"++show s'++" "++ printf "%f" (tp' p ix) ++ "\n")
                           else ("T:"++actionName p a ++":"++
                                       stateName  p s ++":"++
                                       stateName  p s'++" "++ 
                                 show (tp' p ix)++ "\n")

-- Write observation probabilities to a Cassandra-style string
opsToStr :: (POMDP p s a o,Show s, Show a,Show o) => Bool -> p s a o -> String
opsToStr useNames p = concatMap mStr ixs
    where  ixs = [(a,s,o) | a <- actions p
                          , s <- states  p 
                          , o <- observs  p]
           mStr ix@(a,s,o) = if not useNames
                           then ("O:"++show a++":"++show s++":"++show o++" "++printf "%f" (op' p ix)++ "\n")
                           else ("O:"++actionName p a++":"++
                                       stateName  p s++":"++
                                       observName p o++" "++
                                 show (op' p ix)++ "\n")

-- Write rewards to a Cassandra-style string
rwsToStr :: (POMDP p s a o,Show s, Show a,Show o) => Bool -> p s a o -> String
rwsToStr useNames p = concatMap mStr ixs
    where  ixs = [(a,s,s',o) | a  <- actions p
                             , s  <- states  p 
                             , s' <- states  p 
                             , o  <- observs  p]
           mStr ix@(a,s,s',o) = if not useNames
                           then ("R:"++show a++":"++show s++":"++show s'++":"++show o++" "++printf "%f" (rw' p ix)++ "\n")
                           else ("R:"++actionName p a ++":"++
                                       stateName  p s ++":"++
                                       stateName  p s'++":"++
                                       observName p o ++" "++
                                 show (rw' p ix)++ "\n")

