{-
   File	       : $1
   Module      : Cassno.Util
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module Cassno.Util where

-- Normalize list to sum 1.0
normalize :: [Double] -> [Double]
normalize l = map (\v -> v/total) l
    where total = sum l



-- Returns a list of given length with a certain value at a certain index and
-- another value at all other positions

listWithValueAt :: Int -> Int -> a -> a -> [a]
listWithValueAt len index val other = map valAt [0..(len-1)]
    where valAt p = if p == index
                    then val
                    else other

diagMx :: Num a => Int -> a -> a -> [[a]]
diagMx dimension diagVal otherVal = map row [0..(dimension-1)]
    where row p = listWithValueAt dimension p diagVal otherVal

(!!!) :: [[a]] -> (Int,Int) -> a
ls !!! (x0,x1) = (ls !! x0) !! x1

(!!!!) :: [[[a]]] -> (Int,Int,Int) -> a
ls !!!! (x0,x1,x2) = ((ls !! x0) !! x1) !! x2

(!!!!!) :: [[[[a]]]] -> (Int,Int,Int,Int) -> a
ls !!!!! (x0,x1,x2,x3) = (((ls !! x0) !! x1) !! x2) !! x3

-- Checks whether a matrix is diagonal
isDiag :: (Eq a,Num a) => [[a]] -> Bool
isDiag m = all  ((==) 0) $ map ((!!!) m) ixs
    where x0s = [0..(length m)-1]
          x1s = [0..(length $ head m)-1]
          ixs = filter (uncurry (/=)) [(x0,x1) | x0 <- x0s, x1 <- x1s]

isSquareMx :: [[a]] -> Bool
isSquareMx m = all (\r -> length r == dim0) m
    where dim0 = length m

isUniform :: Eq a => [a] -> Bool
isUniform xs = and $ map ((==) $ head xs) (tail xs)

areUniform :: Eq a => [[a]] -> Bool
areUniform mx = isUniform mx && all isUniform mx

-- legacy!
diagOnes :: Int -> [[Double]]
diagOnes d = map (vecWithOneAt d) poss
    where poss = [0..d-1]

-- legacy!
vecWithOneAt :: Int -> Int -> [Double]
vecWithOneAt l ix = map oneAt [0..(l-1)]
    where oneAt p = if p == ix
                    then 1.0
                    else 0.0
