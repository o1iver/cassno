SHELL := /bin/bash

BASEDIR=$(shell pwd)
PROJECT=$(shell basename $(BASEDIR))
VERSION=$(shell cat VERSION)
# Feel free to rewrite this (way too ugly)
AUTHORS=$(shell cat AUTHORS | grep - | cut -d ' ' -f 2- | tr '\n' ',' | sed 's/,/, /g' | sed 's/, \?$$//')
ARCH=$(shell uname -p)
BINNAME="$(PROJECT)-$(ARCH)"

DISTPKG="$(PROJECT)-$(VERSION).tar.gz"

build:
	@echo "Building..."
	ghc --make -outputdir dist/build -hidir dist/build -odir dist/build -isrc -o $(BINNAME) src/Main.hs

info:
	@echo "Project: $(PROJECT)"
	@echo "Version: $(VERSION)"
	@echo "Authors: $(AUTHORS)"
	@echo "Basedir: $(BASEDIR)"
	@echo "Arch:    $(ARCH)"
	@echo "Output:  $(BINNAME)"
	@echo "Distpkg: $(DISTPKG)"
project:
	@echo "$(PROJECT)"
version:
	@echo "$(VERSION)"
authors:
	@echo "$(AUTHORS)"
dist:
	@echo "Creating distribution package..."
	tar -czf $(DISTPKG) *
pkg:
	@echo "Creating distribution package..."
	tar -czf $(DISTPKG) *
clean:
	@echo "Removing *.hi files..."
	find . -name "*.hi" -exec rm -rf {} \;
	@echo "Removing *.o files..."
	find . -name "*.o" -exec rm -rf {} \;
	@echo "Removing dist directory..."
	rm -rf dist/
	@echo "Removing dist package..."
	rm -f $(DISTPKG)
